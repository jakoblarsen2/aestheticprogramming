// applying values for the colours that'll change in an event
var value1 = 255
var value2 = 204
var value3 = 0

var value4 = 120
var value5 = 179
var value6 = 300

// the function set up
function setup() {
  createCanvas(500,500)
}

// the draw function
function draw() {
  background(value4, value5, value6)

// the sun
fill(value1, value2, value3);
ellipse(40,40,150,150)

// grass
  fill(54,134,74)
  rect(0,400,550,100)

// clouds
    fill(255)
    rect(20,50,200,50,50)
    rect(60,70,200,50,50)
    rect(400,30,200,50,50)
    rect(250,10,200,50,50)

// the head and hand
    fill(245,209,148);
      ellipse(330,280,80,90);
      ellipse(330,290,70,90);
      rect(315,300,30,40);
      rect(133,328,30,35,13)
      rect(144,305,7,25,5)
      rect(152,310,7,25,5)
      rect(136,310,7,25,5)

// isolating the rotations for the pinky and thumb
      push()
      rotate(-0.3/2)
        rect(79,335,6,20,5)
      pop()

      push()
      rotate(0.6/2)
      rect(249,260,8,20,5)
      pop()

// the glasses
     fill(0)
        ellipse(348,285,30,30);
        ellipse(312,285,30,30);
        rect(290,285,80,3);

// the eyes
        fill(255,255,255);
        ellipse(348,285,25,25);
        ellipse(312,285,25,25);

// hat, upper body
          fill(255,255,255);
// isolating the rotation for the hat
        push();
        rotate(0.2/-0.6);
        arc(221,360,80,80,PI,0);
        fill(198,21,21);
        rect(181,345,80,15);
        pop();
        arc(330,388,150,100,PI,0);
        rect(255,385,150,150);

// the elbow
        fill(198,21,21);
        arc(210, 408, 60, 60, 0, PI + 0);

// isolating the rotation for the upper arm
        push()
        rotate(1.8/2.0)
        fill(255,255,255);
        rect(450,-10,30,100);
        fill(198,21,21);
        rect(450,54,30,20);
        pop()

// isolating the rotation for the under arm
        push()
        rotate(4.5/1.8);
        fill(255,255,255);
        rect(76,-453,30,75);
        fill(198,21,21)
        rect(76,-435,30,20);
        rect(76,-400,30,20);
        pop()

// putting the red stripes on Holger
           fill(198,21,21);
        arc(330,388,150,105,PI,0);
        rect(255,410,150,30);
        rect(255,460,150,30);
        ellipse(312,230,20,20);

// isolating the rotation for the second arm
        push()
        rotate(-0.3/2)
        fill(255)
        rect(313,435,30,200)
        fill(198,21,21)
        rect(313,460,30,20)
        rect(313,494,30,20)
        rect(313,529,30,20)
        pop()

//mouth
        fill(0)
        rect(325,310,10,2)

// the pupils
        fill(0)
        ellipse(348,285,5,5)
        ellipse(312,285,5,5)

noStroke()

}

// Doing a mousePressed event (From day to night)

// the sky = blue to black
function mousePressed() {

  if(value4 == 120){
    value4 = 30
    value5 = 30
    value6 = 30
  }else{
    value4 = 120
    value5 = 179
    value6 = 300
  }

//the sun = yellow to gray
if(value1 == 255){
  value1 = 180;
  value2 = 180;
  value3 = 180;
}else{
  value1 = 255;
  value2 = 204
  value3 = 0
}
}
