## MiniX1 - Find Walter (Holger)
This is my first time having an actual programming project, so the results reflect that. There were many things and syntaxes I would've loved to include, but wasn't able to.
So here comes a rundown of my code creating Walter.

### Setting the values
Later into the code I've implemented an event, which requires these values. Value 1-3 make the colour yellow for the sun, while the values 4-6 make the light blue for the sky. If I understood it correctly, the values have to be stated/defined even before functions (like setup).

### Function setup
I chose to create a canvas that had the size 500x500. I chose these numbers, because they give me an easy way to quickly "calculate" or estimate the positions of various shapes.

### Function draw - Background Additions
Here I started with some additions to make the background a bit more appealing. First I created a very simple sun, as it only included the ellipse-syntax which I then applied to the coordinates of the top left corner. I then used the fill-syntax to give the sun a yellow colour (my first three values). I made a horizon by adding some grass in the form of a big green rectangle stretching across the lower part of the canvas.
Then I wanted to add some clouds. I added some white rectangles in the upper part of the canvas. Later into the progress, while browsing the various p5js references, I discovered that I can insert a fifth value to the syntax to smoothen the edges of the rectangle.

### Function draw - Walter
I started with the fill-syntax to apply a skin-toned colour. I then used two ellipses and a rectangle to create the head and neck. I then added some more rectangles with smoothened corners to represent fingers. Yesterday, my tutor introduced me to the push- and pop-syntax, which gave me the ability to "isolate" a piece of code. That was necessary for two of the fingers, because is used the rotate-syntax to tilt them a bit. If I hadn't used the push and pop, the rotation would've applied on all the following code. I then made the glasses and the eyes. Here I just stacked ellipses in their right colour.
I then created the hat, which consisted of an ellipse, a rectangle and an arc. The arc really required me to read the reference text properly multiple times, but in the end I got something that looks kind of right. Again, I had to use push and pop to isolate the rotation.
I then used the arc-syntax again to make an elbow, which I then connected with a rectangle on each side, to create an arm.
I then made some red rectangles and laid them on top of Walter's body to make his white sweater a red-white striped sweater.
Again, I used push and pop to rotate the second arm, which is also made of multiple rectangles.
Lastly I made the mouth and pupils, both in black (fill(0)).
I then finished with a noStroke-syntax, to remove the black outer lines of all the shapes.

The order of shapes in my code is kind of all over the place, but it is the only way, I could make the layering work. I tried searching for a syntax that can change "layer-priority" or something similar, but wasn't able to.

### Function mousePressed - Making an event
Actually I was pretty much done at this point. But after seeing some of my classmates incredible work I really wanted to implement something that's moving, so I went on a search for some events on the p5js reference list.
Not being too complicated from the looks of it, I chose the mousePressed-syntax. I wanted to make the picture shift from day to night with a mouse click, so I began implementing the mousePressed-syntax.
I started out with just copying the syntax from the reference list, because it looked pretty easy. But whilst trying to change the colours that are part of the syntax, I realized that I didn't really understand what I was doing and what for. I tried searching for the mousePressed-syntax on youtube, where I quickly found a video that helped me a lot.
In the video, she explained the parameters and their meaning/function and also showed how to use multiple colours.
So I made 6 values for the two colours that I needed and put them all the way to the top. I then carefully followed the syntax' description and inserted the correct numbers and parameters.

## Conclusion
Overall I had really much fun working on this project. I got help from peers, the reference list and a youtube video to make a simple code. Even though it isn't a very complex code, I'm glad with my result, since it's my first time really trying this. I tried compensating for my lack of order with a lot of comments, to quickly see what's going on.
Still, I would've liked to include some more intriguing syntaxes, so I'm excited what the future of this semester holds in store for me!

## Reflection
There is loads to learn from other code. As already stated, I feel like my code isn't very organized and all over the place, so that's something I will look for in other codes to maybe draw inspiration from them.
To me, this coding exercise wasn't very similar to "normal" writing. For me it included that I was aware of the canvas' dimensions all the time. Whenever I wrote some piece of code, I had to think of it in correlation to the 500x500 canvas/coordinate system.

## References
-Video explaining p5js function mousePressed: https://www.youtube.com/watch?v=QEPpgytp4Rg&ab_channel=TaniaPadilla-Brainin

-The p5js reference list: https://p5js.org/reference/

-p5js colour palette: https://editor.p5js.org/simontiger/full/MVVT1T01n


## The Pictures
Here is Walter both at day and night (after a mouse click):
![](holgertag.PNG)

![](holgernacht.PNG)


Executed code: https://jakoblarsen2.gitlab.io/aestheticprogramming/MiniX1/

Code: https://gitlab.com/jakoblarsen2/aestheticprogramming/-/blob/main/MiniX1/sketch.js
