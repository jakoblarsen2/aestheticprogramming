let button;
let checkbox4;
let checkbox3;
let checkbox2;
let checkbox1;
let capture;

function setup(){
  createCanvas(windowWidth,windowHeight)

// creating the base for video capture
capture = createCapture(VIDEO);
capture.size(windowWidth,windowHeight);
capture.hide();

// buttons and checkboxes for cookies-overlay
button = createButton('Accept all');
button.style("color", "#fff");
button.style("background", "#4c69ba");
button.position(width-250, height-200);
button.size(80,35);

button = createButton('Accept selected');
button.style("color", "#fff");
button.style("background", "#4c69ba");
button.position(width-250, height-155);
button.size(80,35);

checkbox4 = createCheckbox('Data 4', false)
checkbox4.style("color", "#fff")
checkbox4.position(width-350, height-160)
checkbox4.size(30,30)
checkbox4.changed(myCheckedEvent)

checkbox3 = createCheckbox('Data 3', false)
checkbox3.style("color", "#fff")
checkbox3.position(width-450, height-160)
checkbox3.size(30,30)
checkbox3.changed(myCheckedEvent)

checkbox2 = createCheckbox('Data 2', false)
checkbox2.style("color", "#fff")
checkbox2.position(width-550, height-160)
checkbox2.size(30,30)
checkbox2.changed(myCheckedEvent)

checkbox1 = createCheckbox('Data 1',false)
checkbox1.style("color", "#fff")
checkbox1.position(width-650, height-160)
checkbox1.size(30,30)
checkbox1.changed(myCheckedEvent)

}

function draw(){
  background(50,50,100)

// measures for the displayed camera
  image(capture, 0,0, width, height);

// asking for cookies
  fill(255)
  stroke(0)
  strokeWeight(2)
  textSize(30)
    text('Accept cookies?', width-580, height-175)

}

// events to write "hidden messages" in console
function myCheckedEvent(){
  if(checkbox1.checked()){
    console.log('We now know your adress')
}else{
  if(checkbox2.checked()){
    console.log('We now know your interests')
}else{
  if(checkbox3.checked()){
    console.log('We just earned money selling this data')
}else{
  if(checkbox4.checked()){
    console.log('We now know everything about you, thanks')

    }
  }
}

}

}
