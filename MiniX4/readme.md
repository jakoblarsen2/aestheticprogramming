[RunMe](https://jakoblarsen2.gitlab.io/aestheticprogramming/MiniX4/)

[The Code](https://gitlab.com/jakoblarsen2/aestheticprogramming/-/blob/main/MiniX4/sketch.js)

## MiniX4 - Datafication

#### Collecting Consent Cowardly
Accept cookies? Want us to better your browsing experience? Questions you see on the daily whilst being online. Sure, you think to yourself, just get this pop up away so you can see the site. Your data/information now belongs to them. But of course that is not what you are told. This project emphasizes the hidden aspects of giving your cookie-consent to these websites. At first it isn't apparent what your giving consent to. If you want to find out for this particular project, take a look in the console. Sometimes, the thing happening to you is hidden, leaving you oblivious to everything.

The reason for the projects name is simple: Big sites are getting thousands of consents for cookies everyday and many of the users are just clueless as to what their accepting. Arguably, this scheme could be called a cowardly move that grew to an utopian extend.

#### The program and how it's "capturing all"
The program isn't nearly where I would've liked it to be. It's a really simple code that is actually not doing too much. I started by using the `createCapture()` syntax to make the webcam work. I tried tinkering with the capture feature a lot and set the dimensions later into the code. I then created multiple buttons and checkboxes. These were the obvious choice for making something that resembles the "usual" cookie-overlay/pop-up. I'm almost certain that I could've made these checkboxes with a `for()` loop, but I wanted to be able to tinker with the individual checkboxes later into the progress. I moved on to the draw function, where I used the `image()` syntax to place the camera capture. I set it to stretch across the whole screen, thus creating an unsettling feeling of being watched while you accept the cookies. It is yourself who is watching, though. Still, I think not many people would appreciate a big picture of themselves on some random website. I also think that people wouldn't appreciate their cookies being sold to strange places around the internet and still no one bats an eye when it comes to this subtle datafication. Arguably, the market for cookies is exploiting humans for their lives. Information getting taken and sold because way too many people don't know the meaning or significance of cookies and their data. As Ulises Mejias and Nick Couldry state in the short essay “Datafication”:


> This infrastructure and those processes are multi-layered and global, including mechanisms for dissemination, access, storage, analysis and surveillance that are owned or controlled mostly by corporations and states.

This goes to show, how large this segment of internet money-making has become. Our data is the main currency of this century, even though you can never be sure of where "your" assets are or where they're being sold to. A scary thought, that the market-controlling currency is being held by few big corporations.

Back to the code, I added some text asking for the consent. Here the "surface-code" is finished.
Next, I have four conditional statements inside a function, that all check if the four checkboxes are checked. For each checkbox being checked, I've added a little message in the console. Hereby I want to imply that for most people it just isn't very transparent or visible as to what your giving consent to (most of the time). I had some troubles working with the checkboxes. They all display their message correctly when being checked individually, but it becomes kind of confusing when just checking and unchecking multiple boxes. There probably is a way to fix that, which I unfortunately was not able to find.


#### What are the cultural implications of data capture?
For many, the data capture of today is not apparent. Many are unbothered and don't know about the significance of the data market. Everything that's online is controlled by it and it holds a very strong position both as a currency and a monopole-tool. People accept cookies and suddenly every site knows which adverts they have to display in order to gain attention. So on one hand, many people actually don't know that their information, their data is being sold and distributed. Then third parties are able to influence these people with ridiculously accurate advertisements and online suggestions. On the other hand, there are people who know about this and try to avoid giving consent to these things.
But no one can deny the fact that the capturing and distribution of data skyrocketed these past few years, and is playing a bigger role than ever before. That being said, there are also no signs of it slowing down. So who knows, maybe data actually will be an official currency in the years to come.

![](Cam.PNG)
