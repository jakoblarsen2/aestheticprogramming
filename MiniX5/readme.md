## **MiniX5** / Auto-Generator

- [RunMe](https://jakoblarsen2.gitlab.io/aestheticprogramming/MiniX5/)
- [The Code](https://gitlab.com/jakoblarsen2/aestheticprogramming/-/blob/main/MiniX5/sketch.js)

### The rules
*Rule 1*. For my first rule I wanted to have something simple and familiar, to allow me to change and modify things along the way. My first rule is pretty much a rule taken from 10Print, that states that when 'x' reaches a certain point, the line will reset a few y-coordinates lower than before. So every time 'x' is bigger than 'x-1', the 'space'-variable will be added to 'y', creating a new line.

*Rule 2*. My second rule sadly did not work how I intended it to, so it isn't part of the code. Right now, the letters on the right side of the canvas are slightly visible. Originally, I wanted them to be invisible and turn visible with the press of a button. I tried working with a `mousePressed()` function that is called back in another function to change the colours of the letters to make them visible. But as I tried various ways of implementing this, it always somehow interfered with the first "10Print-rule". But when I tried putting this in, it also reacted to the `noLoop()` syntax and didn't run. I could not figure out how to apply the `noLoop()` to everything apart from the `mousePressed()` function. So in theory my second rule would've been: Every time a button is pressed, the colour of the text will change.

My program doesn't really "work over time", because of the fact that it stops after having created morse symbols to fill the y-axis on the left side. It doesn't run for a long time, because I set it to not to do so. I could've made it run forever, but at some point there has to be time to translate or look at the morse code.

The description for the MiniX5 stated that a `for()` or `while()` is required. I kind of forgot this until I was finished with the code, so I cheated a bit. I made a for loop that basically just counts to three and then resets. The counting is displayed in the console. So in theory, there is an existing `for()` loop.

### The role of rules and processes
The first of my rules has a pretty crucial role, as it structures the whole concept that I had in mind. I wanted to have on letter / morse symbol on the beginning of each line and the first rule forbids the code to draw more than one symbol per line. The second rule would've made a button on the canvas, thus creating interactivity. So theoretically the second rule would've had the role of unlocking interactivity in my program.

And in my eyes, this is what rules in programs are about. They structure and shape the code into the visions of the creator. With a set of rules stated by the coder, there is full control over what the program will do.

### The Auto-generator
The theme of auto-generated or computed art is very interesting to me. Is the computer the artist that made the code? Or am I, who fully typed in the program, considered the artist when talking about auto-generated art? I think a lot of arguments contradict themselves and it's safe to say that there is not a definitive answer to these questions. Like Jon McCormack et al. wrote in their article "Ten Questions Concerning Generative Computer Art":

> *The goal of programming a machine to be an autonomous artist seems to impose a double standard: We are asking the machine to be autonomous, yet we are also asking for human creativity, assessed by human standards.*

So in my opinion, both are artists in some way or another. The computer does all the work, but the human tells it what to do in the first place. It is a shared process. I think the more boundaries and rules I, as a coder, put into my program, the more I am the artist. Giving the computer more constraints means I'm shaping the code more specific/detailed.

Another interesting topic is randomness. Why do we make things random and are they actually truly random?

> *Randomness is often used to "humanize" or introduce variation and imperfections to an underlying rigid, deterministic process, as when a sequencer program plays back a musical score with slight random timing variations.*

This approach from Jon McCormack et al. is pretty intriguing to me. So me make things random to make them human? We often heard stories about "randomness-algorithms" being perceived as not being random, so the developer had to go into the code and make it less random, to make it *feel* more random. This just means, that a complex code decides what to display next, whilst being observant about not displaying the same things too often in the same time-window. But doesn't that just make the computer or the program *less* human? To "humanize" when using random is a pretty contradicting statement to me, even though I don't have answers to these questions myself.

### References
- P5js Reference List - https://p5js.org/reference/
- 10PRINT - https://10print.org/
- Daniel Shiffman "2D arrays" - https://www.youtube.com/watch?v=OTNpiLUSiB4&ab_channel=TheCodingTrain
- "Ten Questions Concerning Generative Computer Art" - https://www-jstor-org.ez.statsbiblioteket.dk:12048/stable/43834149?sid=primo&seq=2

##### Screenshot of the program
![](RNGpoem.PNG)
