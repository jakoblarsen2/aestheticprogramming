let morse = [".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."]
let letter = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
let alphabet = 26;
// alphabet a-z (26)

let x = 0;
let y = 0;
let space = 50;

function setup(){
  canvas = createCanvas(windowWidth, windowHeight);
  background(60)
  frameRate(10)
}

function draw(){

// telling program how many arrays can be chosen
let randomletter = floor(random(alphabet))

// random morse symbol
fill(83,213,87)
textSize(50)
  text(morse[randomletter], x, y)

// corresponding letter to the morse
fill(68)
textSize(30)
  text(letter[randomletter], width-50, y)

// translations for the console
console.log("A = .-", "B = -...", "C = -.-.", "D = -..", "E = .", "F = ..-.", "G = --.", "H = ....", "I = ..", "J = .---", "K = -.-", "L = .-..", "M = --", "N = -.", "O = ---", "P = .--.", "Q = --.-", "R = .-.", "S = ...", "T = -", "U = ..-", "V = ...-", "W = .--", "X = -..-", "Y = -.--", "Z = --..")

// for loop for miniX5 requirements :)
for (let i = 0; i < 3; i++) {
  console.log(i);
}

// determining space between lines
x += space

// measurements for one morse letter on each line
if(x > x-500){
  x = 0;
  y += space;
}

// discontinuing the loop
if(y > height){
  y = 0
  noLoop()
}

// big text in the "middle"
fill(255)
textSize(50)
  text('Random Generated \n      Morse Poem',width/2.85,height/3)
textSize(20)
text('   take a look in the console \n        for translation sheet\n(or look closely on the right side)', width/2.5, height/2.1)

}
