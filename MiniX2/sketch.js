// declaring/defining the variables
let canvasWidth = 800;
let canvasHeight = 500;
let x;
let y;
let xspeed;
let yspeed;
let circleSize;
let imageSize;
let angry;
let happy;
let angryImage;
let happyImage;
let mood = true;

let c1 = 157;
let c2 = 63;
let c3 = 63;

let c4 = 191;
let c5 = 79;
let c6 = 79;

let c7 = 254;
let c8 = 77;
let c9 = 77;

imageSize = 150;

// preloading the needed images
function preload(){
  angry = loadImage("angry.png");
  happy = loadImage("happy.png");
}

// setup with a canvas with variable values
function setup() {
  createCanvas(canvasWidth,canvasHeight);
}

// defining x & y
// (half of width & height) - (radius of image)
x = canvasWidth/2-imageSize/2;
y = canvasHeight/2-imageSize/2;

// defining the speed variable
xspeed = 2;
yspeed = 2;

function draw() {
// ellipses make the background
background(c7,c8,c9);
fill(c1,c2,c3);
noStroke();
ellipse(700,400,300,300);
ellipse(100,100,600,600);
ellipse(650,50,300,300);
fill(c4,c5,c6);
ellipse(450,200,250,250);
ellipse(170,430,300,300);
ellipse(800,230,200,200);

// applying arguments to the images
happyImage = image(happy, x, y, imageSize, imageSize);
angryImage = image(angry, x, y, imageSize, imageSize);

// setting x & y to move, applying speed
x = x + xspeed;
y = y + yspeed;

// image can't exceed borders
// when hitting a border, direction changes
if(y <= 0 || y + imageSize >= canvasHeight){
  yspeed = -yspeed;
}

if(x <= 0 || x + imageSize >= canvasWidth){
  xspeed = -xspeed;
  }
}

// image changes when mouse is pressed
// colors change when mouse is pressed
function mousePressed() {
  if(mood == true){
    angry = happy;

    c1 = 119;
    c2 = 202;
    c3 = 129;

    c4 = 91;
    c5 = 174;
    c6 = 101;

    c7 = 148;
    c8 = 255;
    c9 = 160;

  }
}
