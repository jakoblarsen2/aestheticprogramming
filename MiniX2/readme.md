Executed code: [This link](https://jakoblarsen2.gitlab.io/aestheticprogramming/MiniX2/)

Code: [This link](https://gitlab.com/jakoblarsen2/aestheticprogramming/-/blob/main/MiniX2/sketch.js)

# MiniX1 - Emoji
For this project, I didn't really have a visual concept in mind before starting or in the early stages of coding. I've actually written about 150 lines of code for my first attempt at making an intriguing emoji before discarding the whole code. It consisted of simple geometric shapes, some colour changes and a conditional statement, that made an ellipse bounce from one corner to the other and back. I just wasn't satisfied with it, because I really wanted to see an improvement from last week. I still wanted to work with this kind of conditional statement, so I also included a similar one in my final code.

### Variables
I started by declaring (and defining some) variables with the `let` syntax. I made many of them, to learn working with them and to have an easy way to change possible mistakes in the values/arguments. Of course I did not declare every single variable before starting to code, but sat them up there whilst getting deeper into the progress.

### Function preload()
After stating the variables, I loaded the needed images. I used the `preload()` function and the `loadImage()` syntax to make that happen. I named my angry emoji "angry" and my happy emoji "happy". I made both emojis with a software similar to photoshop.

### Function setup()
The start was, as usual, creating a Canvas. Here I just inserted my variables for the width and height.

I then defined my x & y. To get an easy idea of where the image is, I just divided width and height by two, to place it in the middle. I then subtracted the image size divided by two, to make the images appear a bit off-centre. That's necessary, so that the images don't just bounce from one corner to the other. Instead, it appears more random when placing them in such a way.

I then defined the "xspeed" and "yspeed" to be 2, which basically just determines how fast the images move.

### Function draw()
I created a background and multiple ellipses on top to make the background a bit more appealing. All colours used here are variables, which made it easy to make the colour change in the end.

The images still needed some info on where to go, so next I applied arguments for both emojis, which were also variables.

I then made my images move with applying my "xspeed" and "yspeed" to my "x" and "y". The image would now just move out of the canvas and into infinity. So what I needed was a conditional statement.
I stated, that `if()` *y* is smaller than or equal to 0 **or** *y* + the image size is bigger than or equal to the canvas height, **then** the "yspeed" changes to "-yspeed", which makes the images go in the opposite direction. I used the same conditional statement for the x-coordinate also.

### Function mousePressed()
Here I made use of a very simple way of making colours (or images) change with a mouse click, that I already used in my miniX1. In the beginning, I stated that mood = true. So here I said that `if()` *mood == true* the image will change from angry to happy. Simultaneously the colours will change from being red-ish to green-ish.

I really wanted it to go back to the angry emoji with a second click, but I just wasn't able to implement it.

### The meaning (concept)
Why are we always angry, sad or dissatisfied? Why don't we ever look at the bright side of things? At times it feels like you are surrounded by bubbles of negative emotions. Sometimes, getting a more positive mindset is actually just as easy as one mouse click. That's exactly the tale the code tries to tell. Thinking collectively, more happiness and positivity does no harm to anyone.
There are no existing emojis to display a shift in moods or thoughts, so this project is also about showing change, even if it's "just" someone's personal thought bubbles or feelings.

### References
-p5js reference list: https://p5js.org/reference/   
-The coding train: https://www.youtube.com/c/TheCodingTrain   
-Help from peers & tutors


### The Images (happy & angry)
![](angry.png)
![](happy.png)
