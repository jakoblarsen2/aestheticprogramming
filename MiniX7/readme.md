[RunMe](https://jakoblarsen2.gitlab.io/aestheticprogramming/MiniX7/)

[The Code](https://gitlab.com/jakoblarsen2/aestheticprogramming/-/blob/main/MiniX7/sketch.js)

## **MiniX7** / Revisit the past

#### Reworking my MiniX4 / "Capture All"
The miniX that I chose to rework is the fourth miniX: Capture All. I chose this one for a very simple reason: It is the one I was dissatisfied with the most. That time I had a stressful week and didn't really manage to get a grasp of the technical and conceptual themes of that week. Also, I learned a few things along the way that I thought could prove useful in regards to this theme.
I did a complete overhaul and the concept behind the project changed quite a bit.

#### What have I changed?
Basically, I changed everything. Originally, I had the camera capture stretched across the whole screen and a "cookies box" with some checkboxes that displayed something in the console when checked.

Now, I have the user choose between three symbols. Depending on which symbol is clicked on, corresponding advertisements will appear on the screen while still leaving the option to click on the other symbols. Therefore, my concept for this project is similar to before. Surveillance capitalism; by giving away data and information about yourself, you not only give these data-collectors money and power, you also make yourself a target for manipulation with accurate advertisements.

Instead of displaying the camera across the whole screen, I implemented the camera and microphone in a way where it is not even being used. Still, the program will ask for permission to use both the microphone and the camera. This adds to the concept of sites wanting to know everything about you.

#### What have I learned this MiniX?
Speaking technically, I tried using object oriented programming again (like last week), because I think that if I learn how to use it properly, it will be a very useful tool for future projects. So I implemented classes and objects and made it work quite easily this time, so this MiniX really helped me to understand the smaller details of this procedure. I also experimented with using pictures as buttons, which actually was easier than I thought and only required a single google-search.

Speaking conceptually, I elaborated on the theme of surveillance capitalism and how data impacts us and our online (or physical) experience. After having read about this topic for the fourth miniX, I found myself paying way more attention to what I'm being asked by websites or programs. The documentary about Shoshana Zuboff on surveillance capitalism also provoked multiple thoughts in me. Do we have control over, what data we give away? Probably not. Is it possible to gain access to the big sites without giving away your data for free? Not really. Questions, speculations, facts and statements like these are more important now than ever before and there is no slowing down from here on out.

#### Aesthetic programming and digital culture
Aesthetic programming is a tool for expressive concepts. The way we code can impact the culture enormously. We've learned many things about coding to include and to be careful about what we express. Everything we see online had to be programmed/coded in some kind of way. So not only is programming an artistic method of expression, but it also has the power to influence masses by simply displaying or exhibiting certain concepts or messages, thereby having "control" over the internet culture.

#### References
Shoshana Zuboff on surveillance capitalism - https://www.youtube.com/watch?v=hIXhnWUmMvw&ab_channel=vprodocumentary

p5js References - https://p5js.org/reference/


![](screenshot.png)
