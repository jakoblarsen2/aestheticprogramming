let button; // button variables
let button2;
let button3;
let ad1 = [] // arrays for ads
let ad2 = []
let ad3 = []
let capture; // camera variable

// loading needed images
function preload() {
  img1 = loadImage("ad1.png");
  img2 = loadImage("adad2.png");
  img3 = loadImage("adadad3.png");

}

function setup(){
  createCanvas(800,600)
  background(50,50,100)

  // creating the image-buttons
  button = createImg("controller.png")
  button.size(150,100)
  button.position(100,250)
  button.mousePressed(changeController)

  button2 = createImg("heart.png")
  button2.size(120,110)
  button2.position(330, 240)
  button2.mousePressed(changeHeart)

  button3 = createImg("tree.png")
  button3.size(130,130)
  button3.position(540,230)
  button3.mousePressed(changeTree)

  // pseudo camera
  capture = createCapture(VIDEO);
  capture.size(640,480);
  capture.hide();

}

function draw(){
  // adding new Ad-objects
  ad1[0] = new Ad(img1,0,0,700,550)
  ad2[0] = new Ad(img2,0,50,700,500)
  ad3[0] = new Ad(img3,0,50,800,500)
}

// functions to show/display the ads
function changeController(){
  ad2[0].showAd()
}

function changeHeart(){
  ad1[0].showAd()
}

function changeTree(){
  ad3[0].showAd()
}


// class for the ads
class Ad {
  constructor(_img, _xpos, _ypos, _width, _height){
    this.img = _img;
    this.xpos = _xpos
    this.ypos = _ypos
    this.width = _width
    this.height = _height
  }

// show function to display ads
showAd(){
  image(this.img, this.xpos, this.ypos, this.width, this.height)
}

}
