let player = [];
let obstacle = [];
let playerSize = 100;
let playerX;
let playerY;
let obstacleX;
let obstacleY;
let obstacleSize;
let d;
let obstacleSpeed = 5;
let dValue = 30;

function setup(){
  createCanvas(windowWidth, windowHeight);
  frameRate(60);

playerX = width/4;
playerY = height/2;
obstacleX = width+30;
obstacleY = height/2;
obstacleSize = 50;

// "showing" the player ellipse
player[0] = new Player(playerX, playerY, playerSize, 50);

}

function draw(){

background(247,187,84);

// spawning a moving obstacle on the right side
if(obstacle.length < 1){
  obstacle.push(new Obstacle(obstacleX, random(50,800), obstacleSize, obstacleSpeed));
}

obstacle[0].showObstacle();
obstacle[0].moveObstacle();
player[0].showPlayer();

// calculating distance between ellipses to determine if they intersect
d = dist(player[0].xpos, player[0].ypos, obstacle[0].xpos, obstacle[0].ypos);

/* if distance < Value = size increases, new obstacle,
distValue increases, ObstacleSpeed increases */
// else a new obstacle will spawn
if(d < dValue){
  obstacle.splice(0,1);
  player[0].size += 10;
  dValue = dValue + 5;
  obstacleSpeed += 1;
}else if(obstacle[0].xpos < 0){
    obstacle.splice(0,1);
  }

textSize(30);
text('EAT THE SMALLER BALL', width/3, height-700);
text('USE ARROW KEYS TO NAVIGATE', width/3.3, height-650);

}


// class for the incoming obstacles
class Obstacle{
  constructor(_xpos, _ypos, _size, _speed){
    this.xpos = _xpos;
    this.ypos = _ypos;
    this.size = _size;
    this.speed = _speed;
  }

showObstacle(){
  fill(random(0,255),random(0,255),random(0,255));
  stroke(0);
  ellipse(this.xpos, this.ypos, this.size);
}

moveObstacle(){
  this.xpos -= this.speed;
}
}


// class for the interactive player ellipse
class Player{
  constructor(_xpos, _ypos, _size, _speed){
    this.xpos = _xpos;
    this.ypos = _ypos;
    this.size = _size;
    this.speed = _speed;
  }

showPlayer(){
  fill(226,224,124);
  stroke(0);
  strokeWeight(5);
  ellipse(this.xpos, this.ypos, this.size);
}

movePlayer(){
  this.xpos += this.speed;
  print("RIGHT");
}

movePlayer2(){
  this.xpos -= this.speed;
  print("LEFT");
}

movePlayer3(){
  this.ypos -= this.speed;
  print("UP");
}

movePlayer4(){
  this.ypos += this.speed;
  print("DOWN");
}

}

// interactive movement for player ellipse
function keyPressed(){
  if (keyCode === RIGHT_ARROW){
    player[0].movePlayer();
}else{
  if (keyCode === LEFT_ARROW){
    player[0].movePlayer2();
}else{
  if (keyCode === UP_ARROW){
    player[0].movePlayer3();
}else{
  if (keyCode === DOWN_ARROW){
    player[0].movePlayer4();

        }
      }
    }
  }
}
