# **MiniX6** / Object Abstraction
- [RunMe](https://jakoblarsen2.gitlab.io/aestheticprogramming/MiniX6/)
- [The Code](https://gitlab.com/jakoblarsen2/aestheticprogramming/-/blob/main/MiniX6/sketch.js)

### The game
The game always shows two objects at a time. First, the player ellipse that can be controlled using the arrow keys and secondly, the obstacle ellipse that moves from the right side of the screen to the left. The objective is to "eat" the smaller ellipse with the controllable player ellipse. Each time an obstacle ellipse get eaten, the new spawning obstacles will move faster, making it more challenging the more you play. If the player misses the obstacle, a new obstacle will spawn as soon as the old one leaves the screen. Also, each time an obstacle has been eaten, the player ellipses radius will increase, making it bigger to balance out the difficulty of eating the accelerating obstacle ellipses. The game will inevitably end when a certain size of the player ellipse is reached.  

![](ObjectGame.PNG)


### The Code
The code consists of the usual `setup()` and `draw()` functions, a `keyPressed()` function and two `classes`. In the setup function I implemented the first of the classes. I "showed" the player ellipse, that can be navigated with the arrow keys with the help of the `keyPressed()` function. In the `draw()` function I spawn the obstacles of the game with `.push`. I sat the obstacle to spawn at a random point on the y-axis, to make the game more challenging. I then applied the necessary functions to the classes.
To detect collision between the ellipses, I used the `dist` method that continuously calculates the distance between the centre of both ellipses. If the distance reaches a certain length, the obstacle ellipse will be determined "eaten" and disappear. In the same conditional statement that makes the obstacle disappear, there are some few extra additions. The players ellipse will increase in size, the required distance value increases by 5 to match the bigger size of the ellipse and the new spawning obstacles will become faster by 1. In both cases of the conditional statement (obstacle eaten or not), a new obstacle will spawn with help of the `splice` method, choosing a new one from the same array.
I then added text with instructions to play the game.

### The Code (Classes)
The "obstacle" class is, obviously, for the obstacle ellipses. I created the values needed as variables in the constructor, so I can fill them in individually when calling upon this class in the code. I added two basic functions ("show" and "move") that make the object visible and gives it the behaviour to move.

Then there is the "player" class, that creates the big, controllable ellipse. The constructor works the same way as before. The main difference between the classes is that I added four different "move" functions (one for each arrow key) to the player class. I call back on these "move" functions in the `keyPressed()` function, corresponding to the correct arrow keys.

### Object Abstraction
Abstraction in computing describes how the attributes and properties of objects create a digital world for us to experience. It determines the look and feel of interfaces and how it is represented. How Matthew Fuller put it:

> In any case, and pace Kay, a computational object is a partial object: the properties and
methods that define it are a necessarily selective and creative abstraction of particular
capacities from the thing it models, and which specify the ways that it can be interacted
with.

Thinking about my own project, I didn't really think of the computer "abstracting" the code through operations. I really just coded and when I made something that worked, it would show. If I wrote some code that didn't work, I would not see anything and get a error message in the console. But when looking at Fullers theories about object abstraction, there is just more to it. It is kind of hard for me to fully grasp the concept, though. Abstraction for me is something that describes things that distance themselves from reality. The computer/machine understands the code and displays what it is being told to display. The processing of the whole procedure is being abstract in regards to using the code and turning it into visible, graphic results. In that sense, there is a form of abstraction.

### References
- Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in Matthew Fuller, How to be a Geek: Essays on the Culture of Software (Cambridge: Polity, 2017).
- p5js Reference - https://p5js.org/reference/
