let yPos = 100;
let speed1 = -3;
let rotation = 0.15;
let rotation1 = 0.001;
let yPos1 = 198;
let timer = 0;

// adding WEBGL to make it 3D
function setup() {
  createCanvas(600, 600, WEBGL);
  frameRate(120);

  }

function draw() {
  background(51, 54, 85);

// for loop to create multiple coins
// push & pop because of stroke()
push();

translate(0,yPos1,0);
  for(let y = 5; y < 200; y+2){
    fill(160,163,191);
    stroke(153,155,179);
    strokeWeight(3);
      cylinder(74,y);
      y += 25;
  }

// two cylinders for the top of the stack
fill(245,189,36);
noStroke();
  cylinder(50,yPos1-10);

fill(194,198,209);
  cylinder(70,yPos1-11);

pop();

// removing stroke for the main coin
noStroke();

// translating to move the coin's position
translate(0,yPos,0);

// framcount times a value sets the speed
// rotating on the x & z axis to flip the coin
rotateX(rotation * frameCount);
rotateZ(rotation1 * frameCount);

// 3 cylinder to make a coin
/* slight height difference so that the colors don't
interfere with each other */
fill(160,163,191);
  cylinder(75,14);

fill(194,198,209);
  cylinder(70,15);

fill(245,189,36);
  cylinder(50,16);

// every frame/"second" adds 1 to timer
  if(frameCount % 60 == 0){
    timer++;
  }

// applying speed to make the coin move
  yPos = yPos + speed1;

// if y reaches -150, direction is reverted
if(yPos <= -150){
  speed1 = speed1 * -1;
  }

/* if y is bigger than 100 and timer is less/equal to 3,
everything sets to 0. If timer is bigger than 3, the old
values are applied again*/
if(yPos > 100){
    if(timer <= 3){
      speed1 = 0;
      rotation = 0;
      rotation1 = 0;
  } else if (timer > 3){
      speed1 = -3;
      rotation = 0.15
      rotation1 = 0.001
      timer = 0

    }
  }

// frameCount resets to reset functions
if(frameCount >= 250){
  frameCount = 0
}

print(timer)

}
