[Executed code](https://jakoblarsen2.gitlab.io/aestheticprogramming/MiniX3/)

[The code](https://gitlab.com/jakoblarsen2/aestheticprogramming/-/blob/main/MiniX3/sketch.js)

## MiniX3 - Designing a throbber
This miniX I discovered and explored the realms of 3D objects in JavaScript. I've never tried to work in a 3D coordinate system, so naturally this project came with a lot of changes and challenges. I started working with a "x-ray type concept", where I placed multiple transparent spheres into each other to represent the earth and its inner structure. I wasn't really satisfied with the results, so I tried exploring other 3D objects and landed on the cylinder. Simultaneously I experimented with the `rotateX()` syntax and came to the conclusion, that my throbber should be a flipping coin (cylinder), as this concept also gives ground for discussions about capitalism and the money-focused morals in culture and society.

![](coinflip.PNG)

### Time-related syntaxes/functions
At first, there wasn't really any relation to time in my code. I spent most of the time trying to make the coin flip and move up, and then down again. I used a `for()` loop to create a stack of coins. I then made the coin that's supposed to flip/move, consisting of 3 cylinders to give it more texture. After a lot of failures and loads of guessing I finally made the coin automatically move and flip up and down again. To challenge myself and to add a little flair to my project, I decided that the coin should lay still on the stack for a short moment, before moving up again. This task really required some time, thinking and guessing, since I had not yet tried to make something time related. At first, I thought it would be as easy as to make the animation appear at the start of the `frameCount` and then resetting the `frameCount` when the animation is done. Of course, it wasn't that easy. So with the help of our instructor, I added a conditional statement that turns framerate into "time".

`if(frameCount % 60 == 0){timer++}`

This conditional statement basically adds 1 to the *timer* variable every time 60 frames have passed. My `frameRate` is set to 120, so a 1 with the *timer* variable would be around half a second.
When the coin has gone through its animation and the *timer* is less or equal to 3, all movement will be stopped, which makes the coin lay still for a little moment. If the *timer* then exceeds 3, all the movement values are applied again. To make sure this statement always works, I set the `frameCount` to reset after 250 counts.

So you could say that the definition of time for this code is an artificially created measurement, that doesn't correlate with the usual perception of time. It's possible to draw a comparison to the computers RTC (Real Time Clock) in the sense that it is a measurement of time, that applies and works for this code, just like the RTC that is "unique" for each computer and isn't hindered by "real" time. Like the Techno-Galactic Software Observatory states in the text "The Techno-Galactic Guide to Software Observation" (Brussels, June 2017), the computers RTC is an isolated piece of perception of time right up until it's synchronized with networks or other influences.

### How does a throbber communicate?
In many cases a throbber or a loading symbol is connected with frustration or boredom, since it only indicates, that you have to wait. So the throbber is in a really unrewarding position, but I still think it holds the ability to express something. Everybody knows the blue spinning circle from Windows or the spinning wheel of death from Apple devices, for some it might even trigger nostalgia. It's safe to say that some few throbbers reached a really wide audience, thus theoretically having the ability to impact or influence people and their feelings.

### Revolving around money
My throbber clearly says something about money. In the beginning, I didn't have any particular idea of what the coin could represent or stand for. But as I coded this flipping coin, I began thinking of the coin as a metaphor for the money-centred society where everything revolves around money. Capitalist or not, there is no denying that money dictates our politics and lives. It can be argued that this throbber represents an endless cycle where the focus always is on financial strength and materialism. The reason to design the coin that way specifically is solely because I grew up in Germany and I'd consider the 2€ coin one of the most recognizable.


### References
p5js references - [p5js References](https://p5js.org/reference/)

Techno-Galactic Guide to Software Observation -
[Link to text](https://observatory.constantvzw.org/tgsoguide_1806051351.pdf)

The Coding Train - [for() Loops](https://www.youtube.com/watch?v=cnRD9o6odjk&t=468s&ab_channel=TheCodingTrain)

The Coding Train - [else, else if....](https://www.youtube.com/watch?v=r2S7j54I68c&ab_channel=TheCodingTrain)
